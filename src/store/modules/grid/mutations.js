import Vue from "vue";

const gridSize = 50;

function isFibonacci(number) {
    if (number === 0) {
        return true;
    } else {
        return !!(isSquare(5 * (number * number) - 4) || isSquare(5 * (number * number) + 4));
    }

}

function isSquare(number) {
    return Math.sqrt(number) % 1 === 0;
}

export default {
    countUp(state, payload) {
        const row = payload.row;
        const column = payload.column;

        state.gridNumbers[row].forEach((cell, index) => {
            if (cell === null) {
                cell = 1;
            } else {
                cell++;
            }
            Vue.set(state.gridNumbers[row], index, cell);
        });

        state.gridNumbers.forEach((gridRow, index) => {
            if (index !== row) {
                let cell = state.gridNumbers[index][column];
                if (cell === null) {
                    cell = 1;
                } else {
                    cell++;
                }
                Vue.set(state.gridNumbers[index], column, cell);
            }
        });
    },

    setInitalGrid(state) {
        state.gridNumbers = [];

        for(let row = 0; row < gridSize; row++) {
            state.gridNumbers.push([]);
            for(let cell = 0; cell < gridSize; cell++) {
                state.gridNumbers[row].push(null);
            }
        }
    },

    removeFibonacci(state) {
        state.gridNumbers.forEach((row, rowIndex) => {
            row.forEach((cell, cellIndex) => {
                if (cell !== null && isFibonacci(cell)) {

                    let fibonacciIndexes = [];
                    fibonacciIndexes.push(cellIndex);
                    let endCheckRange = cellIndex + 5;
                    let lastCheckedCell = cell;


                    for (let startCheckRange = cellIndex + 1; startCheckRange < endCheckRange; startCheckRange++) {
                        let currentCell = state.gridNumbers[rowIndex][startCheckRange];
                        let nextCheckCell = state.gridNumbers[rowIndex][startCheckRange + 1];

                        if (currentCell !== null &&
                            isFibonacci(currentCell) &&
                            (currentCell > lastCheckedCell || ((currentCell === 1 && lastCheckedCell === 1) ^ nextCheckCell === 1)) &&
                            (nextCheckCell > currentCell || fibonacciIndexes.length === 4 || (currentCell === 1 && nextCheckCell === 1) ^ lastCheckedCell === 1)) {

                            fibonacciIndexes.push(startCheckRange);
                            lastCheckedCell = currentCell;
                        } else {
                            break;
                        }
                    }

                    if (fibonacciIndexes.length === 5) {
                        fibonacciIndexes.forEach((fiboCellIndex => {
                            Vue.set(state.gridNumbers[rowIndex], fiboCellIndex, null);
                        }));
                    }
                }
            });
        });
    }
}
