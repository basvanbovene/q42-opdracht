export default {
    getCellByRowAndPosition: (state) => (row, position) => {
        return state.gridNumbers[row][position];
    },
    getGrid: (state) => {
        return state.gridNumbers;
    },
    getCellsByRow: (state) => (rowIndex) => {
        return state.gridNumbers[rowIndex];
    }
}
